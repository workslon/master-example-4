var React = require('react');
var StatusConstants = require('../constants/StatusConstants');
var Link = require('react-router').Link;
var Book = require('./Book.react');

module.exports = React.createClass({
  displayName: 'BookList',

  render: function () {
    var notifications = this.props.notifications || {};
    var status = notifications.status;

    return (
      <div>
        <Link className="create-book btn btn-success bt-sm" to="/books/create">+ Add Book</Link>
        {status === StatusConstants.PENDING && <p className="bg-info">Deleting...</p>}
        {this.props.books.length ?
          <table className="table table-bordered">
            <thead>
              <tr>
                <th>#</th>
                <th>ISBN</th>
                <th>Title</th>
                <th>Year</th>
                <th>Publisher</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              {this.props.books.map((function (book, i) {
                return (
                  <Book key={i} nr={i + 1} book={book} />
                );
              }).bind(this))}
            </tbody>
          </table>
          : <div>Loading books...</div>}
      </div>
    );
  }
});