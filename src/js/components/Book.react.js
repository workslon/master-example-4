var React = require('react');
var Link = require('react-router').Link;
var BookActions = require('../actions/BookActions');
var BookModel = require('../models/Book');

module.exports = React.createClass({
  displayName: 'Book',

  delete: function () {
    BookActions.deleteBook(BookModel, this.props.book);
  },

  render: function () {
    var book = this.props.book,
        publisher = book.publisher || {},
        updatePath = '/books/update/' + book.objectId;

    return (
      <tr>
        <td scope="row">{this.props.nr}</td>
        <td>{book.isbn}</td>
        <td>{book.title}</td>
        <td>{book.year}</td>
        <td>{publisher.name || ''}</td>
        <td className="action-links">
          <Link className="btn btn-primary btn-xs" to={updatePath}>Update</Link>
          <a className="btn btn-danger btn-xs" onClick={this.delete}>Delete</a>
        </td>
      </tr>
    );
  }
});

