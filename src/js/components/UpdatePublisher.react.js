var React = require('react');
var ReactDOM = require('react-dom');
var PublisherActions = require('../actions/PublisherActions');
var PublisherStore = require('../stores/PublisherStore');
var StatusConstants = require('../constants/StatusConstants');
var PublisherModel = require('../models/Publisher');
var IndexLink = require('react-router').IndexLink;

module.exports = React.createClass({
  displayName: 'UpdatePublisher',

  componentDidMount: function () {
    ReactDOM.findDOMNode(this.refs.name).focus();
  },

  componentWillMount: function() {
    this.publisher = PublisherStore.getPublisher(this.props.params.id);
  },

  update: function (e) {
    e.preventDefault();

    var refs = this.refs || {};
    var name = refs.name || {};

    PublisherActions.updatePublisher(PublisherModel, this.publisher, {
      name: name.value
    });
  },

  validate: function(e) {
    PublisherActions.validate(PublisherModel, e.target.id, e.target.value);
  },

  render: function () {
    var notifications = this.props.notifications || {};
    var errors = notifications.errors || {};
    var status = notifications.status;

    return (
        <div>
          <h3>Update Publisher</h3>
          {this.publisher ?
            <form>
              <div className="form-group">
                <label forHtml="name">Name</label>
                <input defaultValue={this.publisher.name} ref="name" type="text" className="form-control" id="name" placeholder="Name" />
              </div>
              <div className="form-group">
                <label forHtml="email">Email</label>
                <span>{this.publisher.email}</span>
                {errors.email && <span className="text-danger">{errors.email}</span>}
              </div>
              <button type="submit" onClick={this.update} className="btn btn-default">Submit</button>
              {status === StatusConstants.SUCCESS && <p className="bg-success">Success!</p>}
              {status === StatusConstants.PENDING && <p className="bg-info">Updating...</p>}
              <IndexLink className="back" to="/publishers/list">&laquo; back</IndexLink>
            </form>
          : <div>No publisher found...</div>}
        </div>
    );
  }
});