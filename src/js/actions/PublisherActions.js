var AppDispatcher = require('../dispatchers/AppDispatcher');
var AppConstants = require('../constants/AppConstants');
var adapter = require('../storage-managers/ParseApiAdapter');

module.exports = {
  validate: function(modelClass, key, value) {
    var errors = {};
    errors[key] = modelClass.check(key, value).message;

    AppDispatcher.dispatch({
      type: AppConstants.PUBLISHER_VALIDATION_ERROR,
      errors: errors
    });
  },

  getPublishers: function (modelClass) {
    var promise = adapter.retrieveAll(modelClass);

    AppDispatcher.dispatchAsync(promise, {
      request: AppConstants.REQUEST_PUBLISHERS,
      success: AppConstants.REQUEST_PUBLISHERS_SUCCESS,
      failure: AppConstants.REQUEST_PUBLISHERS_ERROR
    });
  },

  createPublisher: function (modelClass, data) {
    adapter
      .retrieve(modelClass, '', data.email)
      .then(function(records) {
        try {
          if (records.length) {
            AppDispatcher.dispatch({
              type: AppConstants.NON_UNIQUE_EMAIL
            });
          } else {
            data.runCloudCode = true;
            AppDispatcher.dispatchAsync(adapter.add(modelClass, data), {
              request: AppConstants.REQUEST_PUBLISHER_SAVE,
              success: AppConstants.PUBLISHER_SAVE_SUCCESS,
              failure: AppConstants.PUBLISHER_SAVE_ERROR
            }, data);
          }
        } catch(e) {}
      });

    AppDispatcher.dispatch({
      type: AppConstants.REQUEST_PUBLISHER_SAVE
    });
  },

  updatePublisher: function (modelClass, publisher, newData) {
    var promise;

    newData.runCloudCode = true;
    promise = adapter.update(modelClass, publisher, newData);
    newData.objectId = publisher.objectId;

    AppDispatcher.dispatchAsync(promise, {
      request: AppConstants.REQUEST_PUBLISHER_UPDATE,
      success: AppConstants.PUBLISHER_UPDATE_SUCCESS,
      failure: AppConstants.PUBLISHER_UPDATE_ERROR
    }, newData);
  },

  deletePublisher: function (modelClass, publisher) {
    var promise;

    publisher.runCloudCode = true;
    promise = adapter.destroy(modelClass, publisher);

    AppDispatcher.dispatchAsync(promise, {
      request: AppConstants.REQUEST_PUBLISHER_DESTROY,
      success: AppConstants.PUBLISHER_DESTROY_SUCCESS,
      failure: AppConstants.PUBLISHER_DESTROY_ERROR
    }, publisher);
  }
};