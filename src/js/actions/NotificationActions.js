var AppDispatcher = require('../dispatchers/AppDispatcher');
var AppConstants = require('../constants/AppConstants');
var adapter = require('../storage-managers/ParseApiAdapter');

module.exports = {
  clearNotifications: function () {
    AppDispatcher.dispatch({
      type: AppConstants.CLEAR_NOTIFICATIONS
    });
  }
};